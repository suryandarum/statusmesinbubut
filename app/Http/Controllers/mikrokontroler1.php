<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class mikrokontroler1 extends Controller
{
    public function show()
    {
        $kirim = DB::table('pembacaandata')
            ->orderBy('datetime','desc')->get();
         // dd($kirims);//cek data yang di ambil
        return view('mikrokontroler1.listdata', compact('kirim'));
    }

    public function index()
    {
        //menampilkan data di card
        $kirim = DB::table('pembacaandata')->latest('datetime')->first();


        //menampilkan data grafik tacho
        $kirimgrafik = DB::table('pembacaandata')->get();
        $datagrafiktrpm=[];
        $datagrafikdatetime=[];

        foreach($kirimgrafik as $item){
            $datagrafikrpm[] = $item->datatacho;
            $datagrafikdatetime[] = $item->datetime;
        }
        
        return view('mikrokontroler1.index', compact('kirim','datagrafikrpm','datagrafikdatetime'));

    }
}
