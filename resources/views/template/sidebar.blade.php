<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->

    <!-- Sidebar -->
    <div class="sidebar" id="sticky-sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Hi! {{ Auth::user()->username }} <span class="caret"></span></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-header">DASHBOARD</li>
            <li class="nav-item">
              <a href="/" class="nav-link">
                <i class="nav-icon far fa-calendar-alt"></i>
                <p>
                  Status Mesin Bubut
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a href="/mikrokontroler1/listdata" class="nav-link">
                <i class="nav-icon far fa-calendar-alt"></i>
                <p>
                  Data dari Mikrokontroller
                </p>
              </a>
            </li>


          <li class="nav-header">PENGATURAN</li>
          <button type="button" class="btn btn-success">
            <a href="/register" class="nav-link">
              <p>
                Registrasi Akun
              </p>
            </a>
          </button>

          <button type="button" class="btn btn-danger">
              <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                  {{ __('LOGOUT') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
          </button>
            
  </aside>

  