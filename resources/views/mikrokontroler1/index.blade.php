@extends('template.layout')

@section('judul')
    Status Kerja Mesin Bubut 1
@endsection

@section('isi')


  <div class="card" style="width: 18rem;" col=4>
      <img class="card-img-top" src="{{asset('/imagesupload/mesinbubut.jfif/')}}" alt="Card image cap">
      <div class="card-body">
        <table class="table table-dark">
          <tbody>
            <tr>
              <td>Mesin Bubut</td>
              <td>{{$kirim->idwemos}}</td>
            </tr>
            <tr>
              
              <td>Status Kerja</td>
              <td><span class="badge badge-pill badge-success">{{$kirim->statuskerja}}</span></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <div class="panel">
      <div id="rpm"></div>
    </div>

@endsection

@section('script')
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script>
          Highcharts.chart('rpm', {
          chart: {
              type: 'line'
          },
          title: {
              text: 'Data Sensor Tachometer'
          },
          xAxis: {
            categories: {!!json_encode($datagrafikdatetime)!!},//datasumbux
            title: {
                  text: 'datetime (YYYY/MM/DD HH:MM:SS)'
              }
          },
          yAxis: {
              title: {
                  text: 'Kecepatan Putar (rpm)'
              }
          },
          plotOptions: {
              line: {
                  dataLabels: {
                      enabled: true
                  },
                  enableMouseTracking: true
              }
          },
          series: [{
              data: {!!json_encode($datagrafikrpm)!!}
          }]
      });
  </script>
@endsection