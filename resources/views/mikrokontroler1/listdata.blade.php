@extends('template.layout')

@section('judul')
    pembacaan data dari sensor vibrasi dan tachometer dari mesin bubut1
@endsection

@section('isi')
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">Waktu</th>
            <th scope="col">Id Wemos</th>         
            <th scope="col">Data Vibrasi (m/s^2)</th>
            <th scope="col">Data Tachometer (rpm)</th>
            <th scope="col">Status Kerja Mesin 1</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($kirim as $key=>$value)
                <tr>
                    <td>{{$value->datetime}}</td>
                    <td>{{$value->idwemos}}</td>
                    <td>{{$value->datavibrasi}}</td>
                    <td>{{$value->datatacho}}</td>
                    <td>{{$value->statuskerja}}</td>
                </tr>
            @empty
                <tr colspan="6">
                    <td colspan="3" style="text-align:center"r>No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
@endsection