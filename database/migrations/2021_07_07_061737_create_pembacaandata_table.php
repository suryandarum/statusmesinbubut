<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePembacaandataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembacaandata', function (Blueprint $table) {
            $table->bigIncrements('idpembacaandata');
            $table->dateTime('datetime');
            $table->integer('datavibrasi');
            $table->integer('datatacho');
            $table->enum('statuskerja', ['cutting', 'standby','off']);
            $table->integer('idwemos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembacaandata');
    }
}
