<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJabatanIdjabatanToProfil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profil', function (Blueprint $table) {
            $table->unsignedBigInteger('jabatan_idjabatan');
            $table->foreign('jabatan_idjabatan')->references('idjabatan')->on('jabatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profil', function (Blueprint $table) {
            $table->dropForeign('profil_jabatan_idjabatan_foreign');
            $table->dropColumn('jabatan_idjabatan');
        });
    }
}
